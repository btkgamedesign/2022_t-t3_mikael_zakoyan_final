using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls : MonoBehaviour
{
    [SerializeField] GameObject _Top;
    public GameObject Top => _Top;

    [SerializeField] GameObject _Left;
    public GameObject Left => _Left;

    [SerializeField] GameObject _Bottom;
    public GameObject Bottom => _Bottom;

    [SerializeField] GameObject _Right;
    public GameObject Right => _Right;
}
