using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booster : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Entity")
        {
            Entity entity = other.GetComponent<Entity>();
            if (entity.transform.position.y < transform.position.y)
            {
                entity.BoostUp();
            }
        }
    }
}
