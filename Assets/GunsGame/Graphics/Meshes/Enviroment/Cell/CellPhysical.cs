using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellPhysical : MonoBehaviour
{
    CastCatcher castCatcher;
    [SerializeField] bool _top;
    public bool top => _top;

    [SerializeField] bool _left;
    public bool left => _left;

    [SerializeField] bool _bottom;
    public bool bottom => _bottom;

    [SerializeField] bool _right;
    public bool right => _right;

    [SerializeField] GameObject wallTop;
    [SerializeField] GameObject wallRight;
    [SerializeField] GameObject wallBottom;
    [SerializeField] GameObject wallLeft;

    [SerializeField] GameObject wallTopLeft;
    [SerializeField] GameObject wallTopRight;
    [SerializeField] GameObject wallBottomLeft;
    [SerializeField] GameObject wallBottomRight;

    [SerializeField] GameObject wallLeftRight;
    [SerializeField] GameObject wallTopBottom;

    [SerializeField] GameObject wallTopLeftBottom;
    [SerializeField] GameObject wallTopRightBottom;
    [SerializeField] GameObject wallTopLeftRight;
    [SerializeField] GameObject wallBottomLeftRight;

    [SerializeField] GameObject wallTopLeftBottomRight;

    [SerializeField] Transform wall;

    Walls walls;

    private void Awake()
    {
        castCatcher = GetComponentInChildren<CastCatcher>();
    }

    private void Start()
    {
        transform.position = Snapper.SnapToGrid(transform.position, new Vector2(17f / 2f, 11f / 2f), 1);

        GameObject gen = getGen();
        GameObject instGen = Instantiate(gen, wall);
        StartCoroutine(ScanWithDelay());
        IEnumerator ScanWithDelay()
        {
            yield return null;
            yield return null;
            yield return null;
            walls = instGen.GetComponent<Walls>();

            if (top & castCatcher.Cast(Vector3.up))
            {
                walls.Top.SetActive(false);
            }
            if (bottom & castCatcher.Cast(Vector3.down))
            {
                walls.Bottom.SetActive(false);
            }
            if (left & castCatcher.Cast(Vector3.left))
            {
                walls.Left.SetActive(false);
            }
            if (right & castCatcher.Cast(Vector3.right))
            {
                walls.Right.SetActive(false);
            }
        }
    }

    public Vector3[] GetDoorPositions()
    {
        List<Vector3> result = new List<Vector3>();
        walls = GetComponentInChildren<Walls>();
        if (left & walls.Left)
        {
            if (!walls.Left.activeSelf)
                result.Add(getDirOffset(Vector3.left) + transform.position);
        }
        if (right & walls.Right)
        {
            if (!walls.Right.activeSelf)
            {
                result.Add(getDirOffset(Vector3.right) + transform.position);
            }
        }
        if (top & walls.Top)
        {
            if (!walls.Top.activeSelf)
            {
                result.Add(getDirOffset(Vector3.up) + transform.position);
            }
        }
        if (bottom & walls.Bottom)
        {
            if (!walls.Bottom.activeSelf)
            {
                result.Add(getDirOffset(Vector3.down) + transform.position);
            }
        }
        return result.ToArray();
    }

    Vector3 getDirOffset(Vector3 direction)
    {
        return Vector3.Scale(direction, Room.roomSize / 2);
    }

    Mesh getGenMesh()
    {
        GameObject gen = getGen();
        if (!gen)
        {
            return null;
        }
        return getGen().GetComponent<MeshFilter>().sharedMesh;
    }

    private void OnDrawGizmos()
    {
        Room room = GetComponentInParent<Room>();
        if (!room)
        {
            return;
        }
        if (!room.showGizmos)
        {
            return;
        }

        transform.position = Snapper.SnapToGrid(transform.position, new Vector2(17f/2f, 11f/2f), 1);

        Mesh mesh = getGenMesh();
        if (!mesh)
        {
            return;
        }
        else
        {
            Gizmos.DrawMesh(getGenMesh(), transform.position, transform.rotation);
        }
    }

    GameObject getGen()
    {
        if (top & left & bottom & right)
        {
            return wallTopLeftBottomRight;
        }

        if (top & left & bottom)
        {
            return wallTopLeftBottom;
        }
        if (top & right & bottom)
        {
            return wallTopRightBottom;
        }
        if (top & left & right)
        {
            return wallTopLeftRight;
        }
        if (bottom & left & right)
        {
            return wallBottomLeftRight;
        }

        if (top & left)
        {
            return wallTopLeft;
        }
        if (top & right)
        {
            return wallTopRight;
        }
        if (bottom & left)
        {
            return wallBottomLeft;
        }
        if (bottom & right)
        {
            return wallBottomRight;
        }

        if (top & bottom)
        {
            return wallTopBottom;
        }
        if (left & right)
        {
            return wallLeftRight;
        }

        if (top)
        {
            return wallTop;
        }
        if (right)
        {
            return wallRight;
        }
        if (bottom)
        {
            return wallBottom;
        }
        if (left)
        {
            return wallLeft;
        }

        return null;
    }
}
