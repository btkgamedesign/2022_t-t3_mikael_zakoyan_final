%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: PlayerLegsPelvis
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Pelvis
    m_Weight: 1
  - m_Path: Armature/Pelvis/Belly
    m_Weight: 0
  - m_Path: Armature/Pelvis/Belly/Chest
    m_Weight: 0
  - m_Path: Armature/Pelvis/Belly/Chest/Arm.L
    m_Weight: 0
  - m_Path: Armature/Pelvis/Belly/Chest/Arm.R
    m_Weight: 0
  - m_Path: Armature/Pelvis/Belly/Chest/Head
    m_Weight: 0
  - m_Path: Armature/Pelvis/Belly/Chest/Shoulder.L
    m_Weight: 0
  - m_Path: Armature/Pelvis/Belly/Chest/Shoulder.R
    m_Weight: 0
  - m_Path: Armature/Pelvis/UpperLeg.L
    m_Weight: 1
  - m_Path: Armature/Pelvis/UpperLeg.L/LoverLeg.L
    m_Weight: 1
  - m_Path: Armature/Pelvis/UpperLeg.L/LoverLeg.L/Boot.L
    m_Weight: 1
  - m_Path: Armature/Pelvis/UpperLeg.R
    m_Weight: 1
  - m_Path: Armature/Pelvis/UpperLeg.R/LoverLeg.R
    m_Weight: 1
  - m_Path: Armature/Pelvis/UpperLeg.R/LoverLeg.R/Boot.R
    m_Weight: 1
  - m_Path: Cube
    m_Weight: 0
