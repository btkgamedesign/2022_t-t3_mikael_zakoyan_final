using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    Player player => LevelManager.levelManager.player;
    [SerializeField] Transform cells;
    [SerializeField] Transform playerMap;
    [SerializeField] GameObject mapRoom;
    [SerializeField] GameObject mapDoor;
    [SerializeField] Camera cam;
    Dictionary<Room, MapRoom> mapRooms = new Dictionary<Room, MapRoom>();
    Room[] rooms;

    public void SetCurrentRoom(Room room)
    {
        if (!mapRooms.ContainsKey(room))
        {
            GenerateMap();
        }
        var mr = mapRooms[room].transform.position;
        cam.transform.position = new Vector3(mr.x, mr.y, cam.transform.position.z);
        mapRooms[room].gameObject.SetActive(true);
    }

    private void Update()
    {
        if (player)
        {
            var v = player.transform.position;
            playerMap.transform.localPosition = new Vector3(v.x / Room.roomSize.x, v.y / Room.roomSize.y, 0f);
        }
    }

    public void GenerateMap()
    {
        rooms = FindObjectsOfType<Room>();
        foreach (var room in rooms)
        {
            MapRoom newRoom = Instantiate(mapRoom, cells).GetComponent<MapRoom>();
            var v = room.transform.position;
            newRoom.transform.localPosition = new Vector3(v.x / Room.roomSize.x, v.y / Room.roomSize.y, 0f);
            v = (Vector2)room.size;
            newRoom.transform.localScale = new Vector3(v.x - 0.1f, v.y - 0.1f);
            mapRooms.Add(room, newRoom);
            var exitPositions = room.GetExitLocations();
            foreach (var exitPos in exitPositions)
            {
                GameObject newDoor = Instantiate(mapDoor,cells);
                newDoor.transform.localPosition = new Vector3(exitPos.x / Room.roomSize.x, exitPos.y / Room.roomSize.y);
                newDoor.transform.SetParent(newRoom.transform, true);
            }
            newRoom.gameObject.SetActive(false);
        }
    }

    public void AddDoors()
    {
        foreach (var room in mapRooms)
        {
            var exitPositions = room.Key.GetExitLocations();
            foreach (var exitPos in exitPositions)
            {
                GameObject newDoor = Instantiate(mapDoor, cells);
                newDoor.transform.localPosition = new Vector3(exitPos.x / Room.roomSize.x, exitPos.y / Room.roomSize.y);
                newDoor.transform.SetParent(room.Value.transform, true);
            }
        }
    }

    public void ClearAll()
    {
        mapRooms.Clear();
        for (int i = 0; i < cells.childCount; i++)
        {
            Destroy(cells.GetChild(i).gameObject);
        }
    }
}
