using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Entity")
        {
            var entity = other.GetComponent<Entity>();
            entity.InflictDamage(int.MaxValue);
        }
    }
}
