using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    [SerializeField] float Z;
    public Camera cam { get; set; }
    Player player => LevelManager.levelManager.player;

    public float minX { get; set; }
    public float maxX { get; set; }
    public float minY { get; set; }
    public float maxY { get; set; }

    Vector3 desiredPos;
    public float damping = 10f;

    bool playerThereBefore = false;

    private void Awake()
    {
        minX = float.MinValue;
        maxX = float.MaxValue;
        minY = float.MinValue;
        maxY = float.MaxValue;
    }

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    private void Update()
    {
        bool instant = false;
        if (player == true & playerThereBefore != player)
        {
            instant = true;
        }
        playerThereBefore = player;
        if (!player)
            return;

        desiredPos = new Vector3(player.transform.position.x, player.transform.position.y, Z);
        float orthX = cam.orthographicSize * cam.aspect;
        float x = Mathf.Clamp(desiredPos.x, minX + orthX, maxX - orthX);
        if (Mathf.Abs(maxX - minX) < orthX * 2)
        {
            x = Mathf.Lerp(minX, maxX, 0.5f);
        }

        float orthY = cam.orthographicSize;
        float y = Mathf.Clamp(desiredPos.y, minY + orthY, maxY - orthY);
        desiredPos = new Vector3(
            x,
            y,Z);
        if (instant)
        {
            transform.position = desiredPos;
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, desiredPos, Time.deltaTime * damping);
        }
    }
}
