using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostMagnet : MonoBehaviour
{
    Entity entity;

    private void Awake()
    {
        entity = GetComponentInParent<Entity>();
        if (!entity)
            gameObject.SetActive(false);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Booster")
        {
            PickupBooster pickupBooster = other.GetComponent<PickupBooster>();
            pickupBooster.MagnetTo(entity.transform);
        }
    }
}
