using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityToolbag;

public class Arsenal : MonoBehaviour
{
    public float damageMultiplyer = 1f;

    [SerializeField] Transform _weaponSlot;
    [SerializeField] Transform _source;
    Entity owner;
    Dictionary<int, Weapon> weapons = new Dictionary<int, Weapon>();
    Quaternion lookRotation;

    [SerializeField] bool aimWithDamping;
    [ConditionallyVisible("aimWithDamping")]
    [SerializeField] float aimDamping = 5f;

    int _currentWeapon = 0;
    public int currentWeapon
    {
        get => _currentWeapon;
        set
        {
            if (!weapons.ContainsKey(value))
                return;
            _currentWeapon = value;
            SetCurrentWeapon(value);
        }
    }

    private void Awake()
    {
        owner = GetComponentInParent<Entity>();
    }

    private void Start()
    {
        ScanWeapons();
        currentWeapon = 0;
    }

    private void Update()
    {
        CurrentWeapon().transform.position = _weaponSlot.transform.position;
        CurrentWeapon().transform.rotation = _weaponSlot.transform.rotation;
        _source.transform.rotation = aimWithDamping ? Quaternion.Slerp(_source.rotation, lookRotation, aimDamping * Time.deltaTime) : lookRotation;
    }

    public void LookPoint(Vector3 point)
    {
        LookDirection((point - transform.position).normalized);
    }

    public void LookDirection(Vector3 direction)
    {
        lookRotation = Quaternion.LookRotation(direction);
    }

    void SetCurrentWeapon(int slot)
    {
        foreach (var weaponKey in weapons.Keys)
        {
            weapons[weaponKey].gameObject.SetActive(slot == weaponKey);
        }
    }
    

    public bool CurrentWeaponSlotHasWeapon()
    {
        return weapons.ContainsKey(currentWeapon);
    }

    public Weapon CurrentWeapon()
    {
        return weapons[currentWeapon];
    }
    
    void ScanWeapons()
    {
        weapons = new Dictionary<int, Weapon>();
        for (int i = 0; i < transform.childCount; i++)
        {
            var weapon = transform.GetChild(i).GetComponent<Weapon>();
            if (!weapon)
                continue;
            weapon.owner = owner;
            weapon.overrideSource = _source;
            weapons.Add(weapon.slot, weapon);
        }
    }
}
