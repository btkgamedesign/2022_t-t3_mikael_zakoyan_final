using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Visual : MonoBehaviour
{
    public bool flipX
    {
        get => transform.localScale.x == -1;
        set
        {
            transform.localScale = value ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
        }
    }

    private void Update()
    {
    }
}
