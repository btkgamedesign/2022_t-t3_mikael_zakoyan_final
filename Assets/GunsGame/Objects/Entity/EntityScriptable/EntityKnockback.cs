using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewJump", menuName = "Entity/Knockback")]
public class EntityKnockback : ScriptableObject
{
    public AnimationCurve knockbackCurve;
    public float knockbackMultiplyer = 1f;
}
