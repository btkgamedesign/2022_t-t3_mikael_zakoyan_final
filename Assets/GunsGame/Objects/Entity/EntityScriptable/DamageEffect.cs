using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDamageEffect", menuName = "Entity/Effects/DamageEffect")]
public class DamageEffect : ScriptableObject
{
    public float time;
}
