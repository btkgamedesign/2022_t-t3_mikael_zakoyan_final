using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGravity", menuName = "Entity/Gravity/Gravity")]
public class EntityGravity : ScriptableObject
{
    public float gravityForce = 10;
    public Vector3 gravityDirection = new Vector3(0, -1, 0);
    public AnimationCurve gravityFromZeroRecoveryCurve;
    public float gravityFromZeroTime;
}
