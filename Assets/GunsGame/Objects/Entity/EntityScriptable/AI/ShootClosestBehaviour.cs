using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewShootClosest", menuName = "Entity/Behaviour/ShootClosest")]
public class ShootClosestBehaviour : ScriptableObject
{
    public MinMaxFloat shootFrequency;
    public MinMaxFloat reloadFrequency;
    public MinMaxFloat senseEnemyFrequency;
    public float shootDistance = 10f;
}
