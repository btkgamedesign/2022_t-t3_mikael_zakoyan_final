using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewChase", menuName = "Entity/Behaviour/Chase")]
public class ChaseBehaviour : ScriptableObject
{
    public float YDiffLimit = 0.5f;
    public float Distance = 2;
}
