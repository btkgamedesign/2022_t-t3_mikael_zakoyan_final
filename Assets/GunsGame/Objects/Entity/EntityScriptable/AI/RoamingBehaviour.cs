using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewRoaming", menuName = "Entity/Behaviour/Roaming")]
public class RoamingBehaviour : ScriptableObject
{
    public MinMaxFloat roamStopTime;
    public MinMaxFloat roamStopFrequency;
}
