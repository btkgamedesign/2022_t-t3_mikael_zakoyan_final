using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDash", menuName = "Entity/Dash")]
public class EntityDash : ScriptableObject
{
    public bool canDashWhenAirborn = false;
    public float dashForce = 10;
    public float dashTime = 0.2f;
    public AnimationCurve dashCurve;
}
