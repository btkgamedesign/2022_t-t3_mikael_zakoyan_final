using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGravityWhenAiming", menuName = "Entity/Gravity/WhenAiming")]
public class EntityGravityWhenAiming : ScriptableObject
{
    public AnimationCurve isAimingGravityCurve;
    public float isAimingGravityTime;
}
