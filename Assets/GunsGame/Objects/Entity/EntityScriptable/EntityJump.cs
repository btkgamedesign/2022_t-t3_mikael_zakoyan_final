using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewJump", menuName = "Entity/Jump")]
public class EntityJump : ScriptableObject
{
    public float jumpForce = 8;
    public float jumpTime = 0.4f;
    public AnimationCurve jumpCurve;
}
