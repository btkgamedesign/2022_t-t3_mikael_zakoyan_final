using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityToolbag;

public class StandardEnemy : AI
{
    [SerializeField] RoamingBehaviour roaming;
    [SerializeField] ShootClosestBehaviour shootClosest;
    [SerializeField] bool chaseEnemyOnSeen;
    [ConditionallyVisible("chaseEnemyOnSeen")]
    [SerializeField] ChaseBehaviour chase;

    [SerializeField] float senseFrequency;
    float senseCurrent;

    public override void OnStart()
    {
        base.OnStart(); 
        StartRoamingMove();
        shootClosestBehaviour = shootClosest;
        roamingBehaviour = roaming;
        if (chaseEnemyOnSeen)
        {
            chaseBehaviour = chase;
        }
    }

    public override void EntitySensor()
    {
        base.EntitySensor();
        senseCurrent -= Time.deltaTime;
        if (senseCurrent <= 0f)
        {
            senseCurrent = senseFrequency;
            if (arsenalState == ArsenalState.ShootClosest)
            {
                if (!ScanForEnemies())
                {
                    roamingBehaviour = roaming;
                    StopArsenal();
                    StartRoamingMove();
                }
            }
            else if (ScanForEnemies())
            {
                roamingBehaviour = roaming;
                StartShootingClosestArsenal();
                if (chaseEnemyOnSeen)
                {
                    StartChasing();
                }
                else
                {
                    StartRoamingMove();
                }
            }
        }
    }

    bool ScanForEnemies()
    {
        var entity = entitySensor.GetClosestEntityWithNotTeam(team);
        return entity != null;
    }
}
