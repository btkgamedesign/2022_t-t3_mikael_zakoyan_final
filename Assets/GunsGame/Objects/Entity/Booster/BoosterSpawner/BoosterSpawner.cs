using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterSpawner : MonoBehaviour
{
    [SerializeField] GameObject prefab;
    [SerializeField] int minAmount;
    [SerializeField] int maxAmount;
    [SerializeField] float posRange;
    [SerializeField] AnimationCurve curve;

    private void Start()
    {
        int amount = (int)Mathf.Lerp(minAmount, maxAmount, curve.Evaluate(Random.Range(0f, 1f)));
        for (int i = 0; i < amount; i++)
        {
            Vector3 offset = new Vector3(Random.Range(-posRange / 2, posRange / 2), Random.Range(-posRange / 2, posRange / 2));
            Instantiate(prefab,transform.position + offset, transform.rotation);
        }
    }
}
