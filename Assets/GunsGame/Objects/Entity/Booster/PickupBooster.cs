using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityToolbag;

public class PickupBooster : MonoBehaviour
{
    [SerializeField] Collider collision;
    Rigidbody rb;
    [SerializeField] float spawnRange = 0.1f;
    [SerializeField] float upForce;
    [SerializeField] float angleRange;
    [SerializeField] bool upgradeHealth;
    [ConditionallyVisible("upgradeHealth")]
    [SerializeField] int healthAmount;
    [SerializeField] bool upgradeSpeed;
    [SerializeField] bool upgradeDamage;
    [SerializeField] bool giveScore;
    [SerializeField] float magnetForce;
    [SerializeField] float magnetDrag;
    bool isMagneted;
    Transform target;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        float angle = Random.Range(-angleRange / 2, angleRange / 2);
        transform.Rotate(new Vector3(0, 0, angle));
        transform.Translate(new Vector3(Random.Range(-spawnRange / 2, spawnRange / 2), Random.Range(-spawnRange / 2, spawnRange / 2)));
        rb.AddRelativeForce(Vector3.up * upForce, ForceMode.Impulse);
        rb.AddRelativeTorque(new Vector3(0,0, angle), ForceMode.Impulse);
    }

    private void FixedUpdate()
    {
        if (isMagneted)
        {
            Vector3 dir = target.transform.position - transform.position;
            dir.Normalize();
            rb.AddForce(dir * magnetForce, ForceMode.Force);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Entity")
        {
            Player player = other.GetComponent<Player>();
            if (!player) return;
            Upgrader upgrader = player.GetComponent<Upgrader>();
            if (upgradeHealth)
            {
                if (player.health < player.maxHealth)
                {
                    player.health += healthAmount;
                }
                else
                {
                    upgrader.health++;
                }
            }
            if (upgradeSpeed)
                upgrader.speed++;
            if (upgradeDamage)
                upgrader.damage++;
            if (giveScore)
                LevelManager.levelManager.AddScore();
            Destroy(gameObject);
        }
    }

    public void MagnetTo(Transform transform)
    {
        if (magnetForce <= 0f)
        {
            return;
        }
        rb.useGravity = false;
        rb.drag = magnetDrag;
        target = transform;
        isMagneted = true;
        collision.isTrigger = true;
    }
}
