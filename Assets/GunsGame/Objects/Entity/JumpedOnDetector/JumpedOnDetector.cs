using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpedOnDetector : MonoBehaviour
{
    [SerializeField] int damage;
    [SerializeField] float time;
    [SerializeField] float force;
    Entity entity;

    private void Awake()
    {
        entity = GetComponentInParent<Entity>();
        if (entity == null)
            gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Entity")
        {
            Entity otherEntity = other.GetComponent<Entity>();
            if (otherEntity.team == entity.team)
            {
                return;
            }

            if (otherEntity.characterController.velocity.y < 0f)
            {
                otherEntity.DoKnockback(0, time, force);
                entity.InflictDamage(damage, otherEntity.transform.position, time, force);
            }
            else
            {
                otherEntity.InflictDamage(1, otherEntity.transform.position, time, force);
            }
        }
    }
}
