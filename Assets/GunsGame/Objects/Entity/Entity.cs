using System.Collections.Generic;
using UnityEngine;
using UnityToolbag;
using UnityEngine.Events;
using static UnityEngine.Mathf;

[RequireComponent(typeof(CharacterController))]
public abstract class Entity : MonoBehaviour
{
    [SerializeField] Team _team = Team.Enemy;
    public Team team => _team;
    public int _maxHealth;
    public int maxHealth => _maxHealth;
    public GameObject player { get; set; }
    public CharacterController characterController { get; private set; }
    public Inventory inventory { get; private set; }
    public bool hasInventory => hasThing(inventory);
    public GroundChecker groundChecker { get; private set; }
    public bool hasGroundChecker => hasThing(groundChecker);
    public Arsenal arsenal { get; private set; }
    public bool hasArsenal => hasThing(arsenal);
    public Visual visual { get; private set; }
    public bool hasVisual => hasThing(visual);
    public EntitySensor entitySensor { get; private set; }
    public bool hasEntitySensor => hasThing(entitySensor);
    public Animator animator { get; private set; }
    public bool hasAnimator => hasThing(animator);
    public Upgrader upgrader { get; private set; }
    public bool hasUpgrader => hasThing(upgrader);
    public Vector3 moveDirection { get; private set; }

    public bool affectedByGravity = false;
    [ConditionallyVisible("affectedByGravity")]
    public EntityGravity entityGravity;
    public float gravityForce => entityGravity.gravityForce;
    public Vector3 gravityDirection => entityGravity.gravityDirection;
    public AnimationCurve gravityFromZeroRecoveryCurve => entityGravity.gravityFromZeroRecoveryCurve;
    public float gravityFromZeroTime => entityGravity.gravityFromZeroTime;
    float gravityFromZeroCurTime;

    public bool canJump = false;
    [ConditionallyVisible("canJump")]
    public EntityJump entityJump;
    public float jumpForce => entityJump.jumpForce;
    public float jumpTime => entityJump.jumpTime;
    public AnimationCurve jumpCurve => entityJump.jumpCurve;

    bool isJumping;
    float jumpTimeLeft;
    Vector3 jumpDirection;

    public bool canDash = false;
    [ConditionallyVisible("canDash")]
    public EntityDash entityDash;
    public bool canDashWhenAirborn => entityDash.canDashWhenAirborn;
    public float dashForce => entityDash.dashForce;
    public float dashTime => entityDash.dashTime;
    public AnimationCurve dashCurve => entityDash.dashCurve;

    public bool isDashing { get; private set; }
    float dashTimeLeft;
    Vector3 dashDirection;

    public bool canSprint = false;
    [ConditionallyVisible("canSprint")]
    public float sprintMultiplyer = 1f;
    public bool isSprinting { get; private set; }

    Material myMaterial;
    Renderer[] renderers;
    float dmgTimeLeft;

    public Vector3 move { get; private set; }

    public UnityEvent onDeath;

    int _health;
    public int health
    {
        get => _health;
        set
        {
            if (value <= 0)
                Die();
            else
                _health = Mathf.Clamp(value, 0, maxHealth);
        }
    }

    int _mana;
    int mana
    {
        get => _mana;
        set => _mana = value < 0 ? 0 : value;
    }

    Vector3 _aimDirection;
    public Vector3 aimDirection
    {
        get => _aimDirection;
        set
        {
            Vector3 val = value.normalized;
            val = new Vector3(val.x, val.y);
            _aimDirection = val;
        }
    }
    public Vector3 aimPoint
    {
        set
        {
            Vector3 val = new Vector3(value.x, value.y);
            _aimDirection = (val - transform.position).normalized;
        }
    }

    public bool isAiming { get; private set; }
    [SerializeField] float isAimingMultiplyer;
    [SerializeField] bool slowDownGravityWhenAiming;
    [ConditionallyVisible("slowDownGravityWhenAiming")]
    public EntityGravityWhenAiming gravityWhenAiming;
    public AnimationCurve isAimingGravityCurve => gravityWhenAiming.isAimingGravityCurve;
    public float isAimingGravityTime => gravityWhenAiming.isAimingGravityTime;
    float isAimingGravityCurrentTime;
    bool isAimingGravityAlreadySlowedDown;

    float boostTimeLeft;

    public bool canBeKnockedBack;
    [ConditionallyVisible("canBeKnockedBack")]
    [SerializeField] EntityKnockback knockback;
    public AnimationCurve knockbackCurve => knockback.knockbackCurve;
    public float knockbackMultiplyer => knockback.knockbackMultiplyer;
    int _currentKnockback;
    int currentKnockback
    {
        get => _currentKnockback;
        set
        {
            if (value >= knockbacks.Length)
                _currentKnockback = 0;
            else if (value < 0)
                _currentKnockback = knockbacks.Length;
            else
                _currentKnockback = value;
        }
    }
    Knockback[] knockbacks = new Entity.Knockback[10];



    private void Awake()
    {
        player = GameObject.Find("Player");
        characterController = GetComponent<CharacterController>();
        inventory = GetComponent<Inventory>();
        arsenal = GetComponentInChildren<Arsenal>();
        visual = GetComponentInChildren<Visual>();
        groundChecker = GetComponentInChildren<GroundChecker>();
        entitySensor = GetComponentInChildren<EntitySensor>();
        animator = GetComponentInChildren<Animator>();
        upgrader = GetComponentInChildren<Upgrader>();
        for (int i = 0; i < knockbacks.Length; i++)
        {
            knockbacks[i] = new Knockback();
        }

        renderers = GetComponentsInChildren<Renderer>();
    }

    private void Start()
    {
        health = maxHealth;
        EntityManager.singleton.AddEntity(this);
        moveDirection = Vector3.right;
        OnStart();
    }

    public virtual void OnStart() { }

    private void Update()
    {
        MoveUpdate();
        if (hasInventory)
            Inventory();
        if (hasAnimator)
            Animator();
        if (hasArsenal)
            Arsenal();
        if (hasVisual)
            Visual();
        if (hasEntitySensor)
            EntitySensor();

        if (dmgTimeLeft > 0f)
        {
            foreach (var skinned in renderers)
            {
                skinned.material.SetColor("_Color", Color.red);
            }

        }
        else
        {
            foreach (var skinned in renderers)
            {
                skinned.material.SetColor("_Color", Color.white);
            }
        }
        dmgTimeLeft -= Time.deltaTime;
    }

    public virtual Vector3 Move() { return Vector3.zero; }

    public virtual void Visual() { }

    public virtual void Inventory() { }

    public virtual void Arsenal() 
    {
        arsenal.LookDirection(aimDirection);
    }

    public virtual void EntitySensor() { }

    public virtual void Animator() 
    {
        bool isRun = Mathf.Abs(move.x) > 0.0f;
        animator.SetBool("Run", isRun);
        if (isRun & !isAiming)
        {
            visual.flipX = move.x < 0;
        }
        animator.SetBool("Grounded", groundedCheck());

        animator.SetFloat("Yvel", characterController.velocity.y);

        animator.SetBool("IsAiming", isAiming);
        if (isAiming)
        {
            Vector3 aimDirAbs = new Vector3(Mathf.Abs(aimDirection.x), aimDirection.y);
            animator.SetFloat("LookX", aimDirAbs.x);
            animator.SetFloat("LookY", aimDirAbs.y);

            visual.flipX = aimDirection.x < 0;

            float animWalkSpeed = 0f;
            if (visual.flipX != move.x < 0)
            {
                animWalkSpeed = -1f;
            }
            else
            {
                animWalkSpeed = 1f;
            }

            animator.SetFloat("AimWalkSpeed", animWalkSpeed);
        }

        bool isReloading = false;
        if (hasArsenal)
        {
            isReloading = arsenal.CurrentWeapon().isReloading;
        }
        animator.SetLayerWeight(animator.GetLayerIndex("Aim"), System.Convert.ToInt32((isAiming | isReloading) & Mathf.Abs(move.x) > 0 || isAiming & !groundedCheck()));

        if (hasArsenal)
        {
            animator.SetBool("Reloading", arsenal.CurrentWeapon().isReloading);
        }

        animator.SetLayerWeight(animator.GetLayerIndex("Roll"), System.Convert.ToInt32(isDashing));
    }

    public void InflictDamage(int damage, int dir, float time, float force)
    {
        DoKnockback(dir, time, force);
        InflictDamage(damage);
    }
    public void InflictDamage(int damage, Vector3 source, float time, float force)
    {
        DoKnockback(source, time, force);
        InflictDamage(damage);
    }
    public void InflictDamage(int damage)
    {
        dmgTimeLeft = 0.1f;
        health -= damage;
    }
    void Die() 
    {
        onDeath?.Invoke();
        Destroy(gameObject); 
    }
    public void AddMana(int mana) => this.mana += mana;

    void MoveUpdate()
    {
        if (groundedCheck())
        {
            isAimingGravityAlreadySlowedDown = false;
        }

        bool isReloading = false;
        if (hasArsenal)
        {
            isReloading = arsenal.CurrentWeapon().isReloading;
        }
        float isAimingMultiplyer = (isAiming || isReloading) & groundedCheck() ? this.isAimingMultiplyer : 1.0f;
        float sprintMultiplyer = isSprinting ? this.sprintMultiplyer : 1f;

        move = Move() * isAimingMultiplyer * sprintMultiplyer;
        Vector3 gravity = Vector3.zero;
        if (affectedByGravity)
        {
            gravityFromZeroCurTime -= Time.deltaTime;
            gravityFromZeroCurTime = Mathf.Clamp(gravityFromZeroCurTime, 0f, 1000f);
            isAimingMultiplyer = 1.0f;
            if (slowDownGravityWhenAiming & isAimingGravityCurrentTime >= 0.0f)
            {
                isAimingMultiplyer = isAimingGravityCurve.Evaluate(1 - isAimingGravityCurrentTime / isAimingGravityTime);
                isAimingGravityCurrentTime -= Time.deltaTime;
                gravityFromZeroCurTime = gravityFromZeroTime;
            }
            gravity = gravityDirection.normalized * gravityForce * isAimingMultiplyer 
                * gravityFromZeroRecoveryCurve.Evaluate(1 - gravityFromZeroCurTime/gravityFromZeroTime);
            move = new Vector3(move.x, 0f);
        }

        Vector3 jump = Vector3.zero;
        if (canJump)
        {
            if (isJumping)
            {
                gravity = Vector3.zero;
                float progress = 1 - jumpTimeLeft / jumpTime;
                jump = jumpDirection * jumpForce * jumpCurve.Evaluate(progress) * isAimingMultiplyer;
                jumpTimeLeft -= Time.deltaTime;
                if (jumpTimeLeft <= 0f)
                {
                    EndJump();
                }
            }
            if (groundedCheck() & !isJumping)
            {
                jumpTimeLeft = jumpTime;
            }
        }

        Vector3 dash = Vector3.zero;
        if (canDash)
        {
            if (isDashing)
            {
                if (canDashWhenAirborn)
                {
                    gravity = Vector3.zero;
                    jump = Vector3.zero;
                }
                move = Vector3.zero;
                if (isJumping)
                    EndJump();
                float progress = 1 - dashTimeLeft / dashTime;
                dash = dashDirection * dashForce * dashCurve.Evaluate(progress);
                dashTimeLeft -= Time.deltaTime;
                if (dashTimeLeft <= 0f)
                {
                    isDashing = false;
                }
            }
        }

        if (hasArsenal)
        {
            if (arsenal.CurrentWeapon().freezeOwner)
            {
                if (groundedCheck())
                {
                    move = Vector3.zero;
                }
            }
        }

        if (boostTimeLeft > 0f)
        {
            jump = Vector3.zero;
            gravity = Vector3.up * jumpForce;
            boostTimeLeft -= Time.deltaTime;
        }

        Vector3 totalKnockback = Vector3.zero;
        foreach (var knockback in knockbacks)
        {
            if (knockback.timeLeft <= 0f)
                continue;

            Vector3 dir = Vector3.up;
            if (knockback.direction > 0)
                dir = new Vector3(1, 0, 0);
            if (knockback.direction < 0)
                dir = new Vector3(-1, 0, 0);

            gravity = Vector3.zero;

            totalKnockback += dir * knockback.force * knockbackCurve.Evaluate(1 - knockback.timeLeft / knockback.time);
            knockback.timeLeft -= Time.deltaTime;
        }

        characterController.Move((move + gravity + jump + dash + totalKnockback) * Time.deltaTime);
        
        if (move.magnitude > 0)
        {
            moveDirection = move.normalized;
        }

        if (gravity.magnitude <= 0.0f || groundedCheck())
        {
            gravityFromZeroCurTime = gravityFromZeroTime;
        }

        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
    }

    public bool groundedCheck()
    {
        if (hasGroundChecker)
        {
            if (!groundChecker.isGrounded)
            {
                return false;
            }
        }
        return true;
    }

    public void StartJump(Vector3? jumpDirection = null)
    {
        if (hasArsenal)
        {
            if (arsenal.CurrentWeapon().freezeOwner)
                return;
        }
        if (!groundedCheck()) return;
        if (jumpDirection == null)
        {
            this.jumpDirection = -gravityDirection;
        }
        else
        {
            this.jumpDirection = jumpDirection.Value;
        }
        isJumping = true;
    }

    public void EndJump()
    {
        isJumping = false;
    }

    public virtual void Dash(Vector3? dashDirection = null)
    {
        if (hasArsenal)
        {
            if (arsenal.CurrentWeapon().isReloading)
                arsenal.CurrentWeapon().CancelReload();
            if (hasAnimator)
            {
                animator.SetBool("Reloading", false);
            }
            animator.SetFloat("RunSpeed", isSprinting ? sprintMultiplyer : 1f);
        }
        if (!groundedCheck() & !canDashWhenAirborn)
        {
            return;
        }
        if (dashDirection == null)
        {
            this.dashDirection = moveDirection;
        }
        else
        {
            this.jumpDirection = dashDirection.Value;
        }
        dashTimeLeft = dashTime;
        isDashing = true;
    }

    public void StartAiming()
    {
        if (isAiming) return;
        isAiming = true;
        if (!isAimingGravityAlreadySlowedDown)
        {
            isAimingGravityCurrentTime = isAimingGravityTime;
            isAimingGravityAlreadySlowedDown = true;
        }
    }

    public void StopAiming()
    {
        if (!isAiming) return;
        isAiming = false;
        isAimingGravityCurrentTime = 0.0f;
    }

    private void OnDestroy()
    {
        EntityManager.singleton.RemoveEntity(this);
    }

    bool hasThing(Behaviour thing)
    {
        if (thing == null) return false;
        if (!thing.gameObject.activeSelf) return false;
        return thing.enabled;
    }

    public void BoostUp()
    {
        boostTimeLeft = 0.2f;
    }

    public bool GoDownPlatform()
    {
        bool succeded = false;
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit[] hits = Physics.RaycastAll(ray, characterController.height);
        Debug.DrawRay(ray.origin, ray.direction * characterController.height, Color.red, 3f);
        foreach (var hit in hits)
        {
            OneWayPlatform oneWayPlatform = hit.collider.GetComponent<OneWayPlatform>();
            if (!oneWayPlatform) continue;
            oneWayPlatform.LetEntity(this);
            succeded = true;
            break;
        }
        return succeded;
    }
    
    public void DoKnockback(Vector3 source, float time, float force)
    {
        int dir = 0;
        if (transform.position.x > source.x)
        {
            dir = 1;
        }
        if (transform.position.x < source.x)
        {
            dir = -1;
        }
        DoKnockback(dir, time, force);
    }

    public void DoKnockback(int direction, float time, float force)
    {
        if (!canBeKnockedBack) return;
        knockbacks[currentKnockback].force = force;
        knockbacks[currentKnockback].direction = direction;
        knockbacks[currentKnockback].SetTime(time);
    }

    public void StartSprinting()
    {
        isSprinting = true;
    }

    public void StopSprinting()
    {
        isSprinting = false;
    }

    public enum Team
    {
        Player,
        Enemy
    }

    public enum WeaponAnim
    {
        None,
        Pistol
    }

    class Knockback
    {
        public int direction;
        public float timeLeft;
        public float time;
        public float force;
        public void SetTime(float time)
        {
            this.time = time;
            timeLeft = time;
        }
    }
}
