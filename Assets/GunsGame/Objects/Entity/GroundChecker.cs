using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    public new Collider collider { get; private set; }
    bool _isGrounded;
    public bool isGrounded { get; private set; }

    private void Awake()
    {
        collider = GetComponent<Collider>();
    }

    private void Update()
    {
        isGrounded = _isGrounded;
    }
    private void FixedUpdate()
    {
        _isGrounded = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Ground")
        {
            _isGrounded = true;
        }
    }
}
