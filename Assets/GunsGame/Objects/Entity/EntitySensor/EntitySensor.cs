using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySensor : MonoBehaviour
{
    List<Entity> entitiesInside = new List<Entity>();

    public Entity[] GetEntitiesWithNotTeam(Entity.Team team)
    {
        List<Entity> result = new List<Entity>();
        foreach (var entity in entitiesInside)
        {
            if (entity.team != team)
                result.Add(entity);
        }
        return result.ToArray();
    }

    public Entity GetClosestEntityWithNotTeam(Entity.Team team)
    {
        Entity[] entities = GetEntitiesWithNotTeam(team);
        Entity result = null;
        float minDisntace = float.MaxValue;
        foreach (var entity in entities)
        {
            if (entity == null)
            {
                continue;
            }
            float disance = Vector3.Distance(transform.position, entity.transform.position);
            if (disance < minDisntace)
            {
                minDisntace = disance;
                result = entity;
            }
        }
        return result;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Entity")
        {
            entitiesInside.Add(other.GetComponent<Entity>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Entity")
        {
            entitiesInside.Remove(other.GetComponent<Entity>());
        }
    }
}
