using System.Collections;
using UnityEngine;

public class Player : Entity
{
    public float speed = 10;
    bool aimingCuzShot;
    float aimingCuzShotTimeLeft;

    public override Vector3 Move()
    {
        base.Move();

        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        bool noJump = false;
        if (Input.GetKeyDown(KeyCode.Space) & vertical < 0f)
        {
            noJump = GoDownPlatform();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!noJump)
            {
                StartJump();
            }
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            EndJump();
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Dash();
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            LevelManager.levelManager.Regenerate();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            StartSprinting();
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            StopSprinting();
        }

        transform.localScale = Vector3.one * Mathf.Clamp((float)health / 100f, 0.5f, 1.5f);

        return new Vector3(horizontal, vertical, 0) * speed;
    }

    public override void Inventory()
    {
        base.Inventory();
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            inventory.UseIteam(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            inventory.UseIteam(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            inventory.UseIteam(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            inventory.UseIteam(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            inventory.UseIteam(5);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            inventory.UseIteam(6);
        }
    }

    public override void Arsenal()
    {
        base.Arsenal();
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            arsenal.currentWeapon = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            arsenal.currentWeapon = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            arsenal.currentWeapon = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            arsenal.currentWeapon = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            arsenal.currentWeapon = 5;
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            arsenal.currentWeapon = 6;
        }


        if (arsenal.CurrentWeaponSlotHasWeapon())
        {
            if (Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyDown(KeyCode.Mouse0) & !isAiming)
            {
                StartAiming();
                aimingCuzShot = Input.GetKeyDown(KeyCode.Mouse0) & !Input.GetKeyDown(KeyCode.Mouse1);
            }

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (aimingCuzShot)
                {
                    aimingCuzShotTimeLeft = groundedCheck() ? arsenal.CurrentWeapon().getCooldown * 2 : 0.1f;
                }
            }

            if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                StopAiming();
                aimingCuzShot = false;
            }

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                IEnumerator slightDelay()
                {
                    yield return null;
                    yield return null;
                    arsenal.CurrentWeapon().PressTrigger();
                }

                if (aimingCuzShot)
                {
                    StartCoroutine(slightDelay());
                }
                else
                {
                    arsenal.CurrentWeapon().PressTrigger();
                }
            }

            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                arsenal.CurrentWeapon().ReleaseTrigger();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                arsenal.CurrentWeapon().Reload();
            }

            if ((Mathf.Abs(move.x) > 0 || !groundedCheck()) & aimingCuzShot & aimingCuzShotTimeLeft < 0f)
            {
                StopAiming();
                aimingCuzShot = false;
            }

            aimingCuzShotTimeLeft -= Time.deltaTime;
        }

        if (isAiming)
        {
            aimDirection = LevelManager.levelManager.GetGlobalMousePosition() - transform.position;
        }

        arsenal.LookPoint(LevelManager.levelManager.GetGlobalMousePosition());
    }

    public override void Dash(Vector3? dashDirection = null)
    {
        base.Dash(dashDirection);
        if (aimingCuzShot)
        {

            StopAiming();
            aimingCuzShot = false;
            aimingCuzShotTimeLeft = 0f;
        }
        base.Dash(dashDirection);
    }
}
