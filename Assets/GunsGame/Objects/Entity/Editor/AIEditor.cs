using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AI))]
public class AIEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var trg = target as AI;
        if (GUILayout.Button("Start Roaming"))
        {
            trg.StartRoamingMove();
        }
        if (GUILayout.Button("Stop All"))
        {
            trg.StopMove();
        }
        if (GUILayout.Button("Start shooting closest"))
        {
            trg.StartShootingClosestArsenal();
        }
    }
}
