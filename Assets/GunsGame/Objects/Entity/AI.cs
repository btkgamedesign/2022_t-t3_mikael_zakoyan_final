using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : Entity
{
    bool insideTLcorner;
    bool insideTRcorner;

    public float speed;
    public MoveState moveState { get; private set; }
    public ArsenalState arsenalState { get; private set; }

    public RoamingBehaviour roamingBehaviour { get; set; }
    [Header("Roaming behaviour")]
    int roamDirection = 0;
    public MinMaxFloat roamStopTime => roamingBehaviour.roamStopTime;
    float currentRoamStopTime;
    public MinMaxFloat roamStopFrequency => roamingBehaviour.roamStopFrequency;
    float currentRoamStopFrequency;

    public ShootClosestBehaviour shootClosestBehaviour { get; set; }
    public MinMaxFloat shootFrequency => shootClosestBehaviour.shootFrequency;
    float currentShootFrequency;
    public MinMaxFloat reloadFrequency => shootClosestBehaviour.reloadFrequency;
    float currentReloadFrequency;
    public MinMaxFloat senseEnemyFrequency => shootClosestBehaviour.senseEnemyFrequency;
    public float shootDistance => shootClosestBehaviour.shootDistance;
    float currentSenseEnemyFrequency;
    Entity shootClosestTarget = null;


    public ChaseBehaviour chaseBehaviour { get; set; }
    public float ChaseYDiffLimit => chaseBehaviour.YDiffLimit;
    public float chaseDistance => chaseBehaviour.Distance;


    public override Vector3 Move()
    {
        base.Move();
        switch (moveState)
        {
            case MoveState.None:
                return Vector3.zero;
            case MoveState.Roaming:
                return MoveRoaming();
            case MoveState.Chasing:
                return MoveChase();
            default:
                return Vector3.zero;
        }
    }

    public override void Arsenal()
    {
        base.Arsenal();
        switch (arsenalState)
        {
            case ArsenalState.None:
                break;
            case ArsenalState.ShootClosest:
                ArsenalShootClosest();
                break;
            default:
                break;
        }
    }

    public override void EntitySensor()
    {
        base.EntitySensor();
        if (arsenalState == ArsenalState.ShootClosest || moveState == MoveState.Chasing)
        {
            EntitySensorScanForClosest();
        }
    }

    #region roaming
    public void StartRoamingMove()
    {
        moveState = MoveState.Roaming;
    }

    Vector3 MoveRoaming()
    {
        void RandomizeRoam()
        {
            var randomRoll = UnityEngine.Random.Range(0, 2);
            roamDirection = randomRoll == 0 ? -1 : 1;
        }

        if (insideTRcorner || insideTLcorner)
        {
            roamDirection = Convert.ToInt32(insideTLcorner) - Convert.ToInt32(insideTRcorner);
        }
        if (!insideTLcorner & !insideTRcorner & roamDirection == 0)
        {
            RandomizeRoam();
        }

        if (currentRoamStopFrequency <= 0f)
        {
            currentRoamStopTime = MinMax.GetRandom(roamStopTime);
            currentRoamStopFrequency = MinMax.GetRandom(roamStopFrequency);
        }
        if (currentRoamStopTime <= 0f & currentRoamStopFrequency <= 0f)
        {
            currentRoamStopFrequency = MinMax.GetRandom(roamStopFrequency);
        }
        if (currentRoamStopTime <= 0f)
        {
            currentRoamStopFrequency -= Time.deltaTime;
        }

        currentRoamStopTime -= Time.deltaTime;

        if (currentRoamStopTime >= 0)
        {
            roamDirection = 0;
        }

        return Vector3.right * speed * roamDirection;
    }
    #endregion

    #region chase
    Vector3 MoveChase()
    {
        if (!entitySensor)
        {
            return moveDirection * speed;
        }
        if (shootClosestTarget == null)
        {
            return Vector3.zero;
        }
        float distance = Vector3.Distance(transform.position, shootClosestTarget.transform.position);
        if (distance < chaseDistance)
        {
            return Vector3.zero;
        }

        Vector3 targetDirection = shootClosestTarget.transform.position - transform.position;
        targetDirection.Normalize();
        if (Mathf.Abs(targetDirection.y) > ChaseYDiffLimit)
        {
            return Vector3.zero;
        }
        targetDirection.y = 0f;
        targetDirection *= 1000f;
        targetDirection.Normalize();

        return targetDirection * speed;
    }

    public void StartChasing()
    {
        moveState = MoveState.Chasing;
    }
    #endregion
    public void StopMove()
    {
        moveState = MoveState.None;
    }

    #region shooting closest
    public void StartShootingClosestArsenal()
    {
        arsenalState = ArsenalState.ShootClosest;
    }

    float distanceToTarget;
    void ArsenalShootClosest()
    {

        if (distanceToTarget < shootDistance)
        {
            StartAiming();
        }
        else
        {
            StopAiming();
        }

        if (hasEntitySensor)
        {
            if (shootClosestTarget == null)
            {
                aimDirection = Vector3.down;
                return;
            }
            else
            {
                distanceToTarget = Vector3.Distance(transform.position, shootClosestTarget.transform.position);
            }
            aimPoint = shootClosestTarget.transform.position;
        }
        else
        {
            aimDirection = moveDirection;
        }

        if (currentShootFrequency <= 0 & isAiming)
        {
            arsenal.CurrentWeapon().PressTrigger();
            arsenal.CurrentWeapon().ReleaseTrigger();
            currentShootFrequency = MinMax.GetRandom(shootFrequency);
        }
        currentShootFrequency -= Time.deltaTime;

        if (currentReloadFrequency <= 0)
        {
            arsenal.CurrentWeapon().Reload();
            currentReloadFrequency = MinMax.GetRandom(reloadFrequency);
        }
        currentReloadFrequency -= Time.deltaTime;


    }

    void EntitySensorScanForClosest()
    {
        if (currentSenseEnemyFrequency <= 0f)
        {
            shootClosestTarget = entitySensor.GetClosestEntityWithNotTeam(team);
            currentSenseEnemyFrequency = MinMax.GetRandom(senseEnemyFrequency);
        }
    }
    #endregion
    public void StopArsenal()
    {
        arsenalState = ArsenalState.None;
    }

    private void OnTriggerEnter(Collider other)
    {
        OnTriggerCorner(other, true);
    }

    private void OnTriggerExit(Collider other)
    {
        OnTriggerCorner(other, false);
    }

    void OnTriggerCorner(Collider other, bool inside)
    {
        if (other.tag == "Corner")
        {
            var corner = other.GetComponent<Corner>();
            switch (corner.side)
            {
                case Corner.Side.TL:
                    insideTLcorner = inside;
                    break;
                case Corner.Side.TR:
                    insideTRcorner = inside;
                    break;
            }
        }
    }

    public enum MoveState
    {
        None,
        Roaming,
        Chasing 
    }

    public enum ArsenalState
    {
        None, 
        ShootClosest
    }

}
