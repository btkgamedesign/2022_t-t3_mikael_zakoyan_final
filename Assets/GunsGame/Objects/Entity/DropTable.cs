using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTable : MonoBehaviour
{
    public Drop[] drops;

    public void Execute()
    {
        float roll = Random.Range(0f, 1f);
        List<GameObject> dropsObjects = new List<GameObject>();
        foreach (var drop in drops)
        {
            if (drop.isInRange(roll))
            {
                dropsObjects.Add(drop._object);
            }
        }
        foreach (var dropObj in dropsObjects)
        {
            Instantiate(dropObj, transform.position, transform.rotation);
        }
    }
    
    [System.Serializable]
    public class Drop
    {
        public GameObject _object;
        public float chanceMin;
        public float chanceMax;
        public bool isInRange(float value)
        {
            return value >= chanceMin & value <= chanceMax;
        }
    }
}
