using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static LevelManager;

public class Upgrader : MonoBehaviour
{
    Entity entity;
    AI ai;
    Player player;
    Randomizer rand => levelManager.randomizer;

    [SerializeField] bool basedOnLevelLevel;

    [SerializeField] float speedPercentIncrease = 0.10f;
    float baseSpeed;
    int _speed;
    public int speed 
    { 
        get => _speed;
        set
        {
            if (ai)
            {
                ai.speed = baseSpeed + baseSpeed * speedPercentIncrease * value;
            }
            if (player)
            {
                player.speed = baseSpeed + baseSpeed * speedPercentIncrease * value;
            }
            _speed = value;
        }
    }

    [SerializeField] float damagePercentIncrease = 0.10f;
    int _damage;
    public int damage
    {
        get => _damage;
        set
        {
            if (entity.hasArsenal)
            {
                entity.arsenal.damageMultiplyer = 1 + damagePercentIncrease * value;
                _damage = value;
            }
        }
    }

    [SerializeField] float healthPercentIncrease = 0.30f;
    int baseHealth;
    int _health;
    public int health
    {
        get => _health;
        set
        {
            entity._maxHealth = baseHealth + (int)(healthPercentIncrease * baseHealth * value);
            _health = value;
        }
    }


    private void Awake()
    {
        entity = GetComponent<Entity>();
        ai = GetComponent<AI>();
        if (ai)
            baseSpeed = ai.speed;
        player = GetComponent<Player>();
        if (player)
            baseSpeed = player.speed;
        if (levelManager == null)
        {
            return;
        }
        baseHealth = entity.maxHealth;
        speed = levelManager.levelSpeed;
        damage = levelManager.levelDamage;
        health = levelManager.levelHealth;
    }
}
