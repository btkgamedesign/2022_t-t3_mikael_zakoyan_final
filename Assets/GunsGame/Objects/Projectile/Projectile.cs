using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityToolbag;
public class Projectile : MonoBehaviour
{
    public Entity _owner;
    public Entity owner
    {
        get => _owner;
        set
        {
            team = value.team;
            _owner = value;
        }
    }
    public Entity.Team? team = null;
    public bool hasOwner => owner != null;
    [SerializeField] float lifetime = 10;
    float life;
    public float speed;
    public int damage;
    public int critDamage;
    [SerializeField] private bool homing;
    [ConditionallyVisible("homing")]
    [SerializeField] private float homingDamping = 1;
    Entity closestEntityHoming;
    Quaternion desiredRotation;
    [SerializeField] GameObject subEmmited;
    [SerializeField] bool preserveAfterDamage;
    [SerializeField] bool canBeDestroyedByOtherProjectiles;
    [SerializeField] bool goThroughWalls;
    public bool isCritical { get; set; }
    [SerializeField] bool scaleOverTime;
    [ConditionallyVisible("scaleOverTime")]
    [SerializeField] float scaleSpeed;

    [SerializeField] bool doKnockback;
    [ConditionallyVisible("doKnockback")]
    [SerializeField] float time;
    [ConditionallyVisible("doKnockback")]
    public float force;

    private void Start()
    {
        life = lifetime;
        Vector3 forwardLocal = transform.rotation * Vector3.forward;
        forwardLocal.z = 0f;
        forwardLocal.Normalize();
        transform.rotation = Quaternion.LookRotation(forwardLocal);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);

        if (homing)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, 500f);
            List<Entity> entitiesNearby = new List<Entity>();
            foreach (var collider in colliders)
            {
                if (collider.gameObject.tag == "Entity")
                {
                    entitiesNearby.Add(collider.gameObject.GetComponent<Entity>());
                }
            }
            Entity closestEntity = null;
            float minDistance = float.MaxValue;
            foreach (var entity in entitiesNearby)
            {
                if (team != null)
                {
                    if (entity.team == team.Value)
                    {
                        continue;
                    }
                }
                float distance = Vector3.Distance(transform.position, entity.transform.position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestEntity = entity;
                }
            }
            closestEntityHoming = closestEntity;
        }
    }

    private void Update()
    {
        life -= Time.deltaTime;
        if (life <= 0.0f)
        {
            Destroy(gameObject);
        }

        transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);
        if (homing & closestEntityHoming != null)
        {
            desiredRotation = Quaternion.LookRotation
                ((closestEntityHoming.transform.position - transform.position).normalized);
            transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, Time.deltaTime * homingDamping);
            transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
        }

        if (scaleOverTime)
        {
            transform.localScale += Vector3.one * Time.deltaTime * scaleSpeed;
        }
    }

    public void Destr()
    {
        if (subEmmited != null)
        {
            Instantiate(subEmmited, transform.position, transform.rotation);
        }
        if (!preserveAfterDamage)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Entity")
        {
            Entity otherEntity = other.GetComponent<Entity>();
            if (team != null)
            {
                if (team.Value == otherEntity.team)
                {
                    return;
                }
            }
            if (otherEntity.isDashing)
            {
                return;
            }

            var enemy = other.GetComponent<Entity>();
            int finalDamage = isCritical ? critDamage : damage;
            if (doKnockback)
                enemy.InflictDamage(finalDamage, transform.position, time, force);
            else
                enemy.InflictDamage(finalDamage);
            Destr();
        }
        if (other.tag == "Ground" & !goThroughWalls)
        {
            OneWayPlatform isOneWayPlatform = other.GetComponentInParent<OneWayPlatform>();
            if (isOneWayPlatform)
                return;
            Destr();
        }
        if (other.tag == "Projectile" & canBeDestroyedByOtherProjectiles)
        {
            var proj = other.GetComponent<Projectile>();
            proj.Destr();
            if (!preserveAfterDamage)
            {
                Destr();
            }
        }
    }

}
