using UnityEngine;

public class RadialSpawner : MonoBehaviour, IEntitySpawner
{
    [SerializeField] private float radius;
    public void Spawn(GameObject entity)
    {
        var instance = Instantiate(entity);
        instance.transform.position = GetSpawnPos();
    }

    Vector3 GetSpawnPos() => (new Vector3
            (randRadius(), randRadius(), randRadius())).normalized * randRadius() * 2 + transform.position;
    

    float randRadius() => Random.Range(-radius / 2, radius / 2);

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.2f, 0.2f, 0.2f, 0.1f);
        Gizmos.DrawSphere(transform.position, radius);
        Gizmos.color = new Color(0.4f, 0.4f, 0.4f);
        Gizmos.DrawWireSphere(transform.position, radius);
    }

    [SerializeField] private GameObject sample;
    public void SpawnSample() => Spawn(sample);
    
}
