using UnityEngine;

public interface IEntitySpawner
{
    void Spawn(GameObject enemy);
}