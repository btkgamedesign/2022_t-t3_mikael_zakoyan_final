using UnityEngine;

public class HelathIteam : Iteam
{
    [SerializeField] int healAmount;
    public override void OnUseEffect(Entity user)
    {
        base.OnUseEffect(user);
        user.InflictDamage(-healAmount);
    }
}
