using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.SceneManagement.SceneManager;
using static UnityEngine.Application;

public class MainMenu : MonoBehaviour
{
    public void Play() => LoadScene(1);
    public void QuitGame() => Quit();
}
