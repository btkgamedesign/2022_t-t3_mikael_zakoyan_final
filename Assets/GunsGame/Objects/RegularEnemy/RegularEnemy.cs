using UnityEngine;

public class RegularEnemy : Entity
{
    [SerializeField] private float speed;
    public override Vector3 Move()
    {
        base.Move();
        return Vector3.forward * speed;
    }
}
