using UnityEngine;

public class InventoryCollector : MonoBehaviour
{
    Inventory inventory;

    private void Awake()
    {
        inventory = GetComponentInParent<Inventory>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Iteam")
        {
            var iteam = other.GetComponent<Iteam>();
            iteam.Collect(inventory.owner);
        }
    }
}
