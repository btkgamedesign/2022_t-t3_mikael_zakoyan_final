using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.SceneManagement.SceneManager;

public class LevelManager : MonoBehaviour
{
    public static LevelManager levelManager;
    public MainCamera mainCamera { get; private set; }
    public Map map { get; private set; }
    public Player player { get; private set; }
    public Generator generator { get; private set; }
    public Randomizer randomizer { get; private set; }

    [SerializeField] int upgradeIterations = 3;
    [SerializeField] GameObject playerPrefab;
    Room[] allRooms;
    Room activeRoom;

    public int levelSpeed { get; private set; }
    public int levelDamage { get; private set; }
    public int levelHealth { get; private set; }

    Room centerRoom;

    public int score { get; private set; }

    private void Awake()
    {
        levelManager = this;
        mainCamera = Camera.main.GetComponent<MainCamera>();
        player = FindObjectOfType<Player>();
        map = FindObjectOfType<Map>();
        generator = FindObjectOfType<Generator>();
        randomizer = FindObjectOfType<Randomizer>();
    }

    private void Start()
    {
    }

    void SetCameraLimits()
    {

    }

    public void Regenerate()
    {
        for (int i = 0; i < upgradeIterations; i++)
        {
            int roll = randomizer.Int(3);
            if (roll == 0)
                levelHealth++;
            if (roll == 1)
                levelDamage++;
            if (roll == 2)
                levelSpeed++;
        }
        ClearAll();
        generator.Generate();
        upgradeIterations++;
    }

    void ClearAll()
    {
        player.gameObject.SetActive(false);
        generator.ClearAll();
        map.ClearAll();
    }

    public void NotifyDoneGenerating()
    {
        allRooms = FindObjectsOfType<Room>();
        centerRoom = null;
        float minValue = float.MaxValue;
        foreach (var room in allRooms)
        {
            if (room.transform.position.x < minValue)
            {
                minValue = Vector3.Distance(room.transform.position, Vector3.zero);
                centerRoom = room;
            }
        }
        Vector3 spawn = centerRoom.getSpawnPos(0.5f);
        centerRoom.removeEntitiesOnStart = true;
        if (player != null)
        {
            player.gameObject.SetActive(true);
        }
        else
        {
            player = Instantiate(playerPrefab).GetComponent<Player>();
        }
        player.transform.position = spawn + Vector3.up * player.characterController.height;
        player.onDeath.AddListener(() => OnDeath());
    }

    public Vector3 GetGlobalMousePosition()
    {
        return mainCamera.cam.ScreenToWorldPoint(Input.mousePosition 
            + Vector3.forward * -mainCamera.transform.position.z);
    }

    public void SetActiveRoom(Room room)
    {
        map.SetCurrentRoom(room);
        foreach (var rom in allRooms)
        {
            if (rom == room)
            {
                rom.Activate();
            }
            else
            {
                rom.Deactivate();
            }
        }
    }

    public void AddScore()
    {
        score++;
    }

    public void OnDeath()
    {
        StartCoroutine(IE());

        IEnumerator IE()
        {
            yield return new WaitForSeconds(2f);
            LoadScene(0);
        }
    }
}
