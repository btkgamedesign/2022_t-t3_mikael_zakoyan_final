using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPlatform : MonoBehaviour
{
    [SerializeField] Collider collision;
    List<Entity> entitiesWithFreePass = new List<Entity>();

    private void OnTriggerEnter(Collider other)
    {
        OnTrigger(other);
    }

    private void OnTriggerExit(Collider other)
    {
        OnTrigger(other);
    }

    void OnTrigger(Collider other)
    {
        if (other.tag == "Entity")
        {
            Entity entity = other.gameObject.GetComponent<Entity>();
            if (entitiesWithFreePass.Contains(entity))
            {
                entitiesWithFreePass.Remove(entity);
                return;
            }
            Physics.IgnoreCollision(entity.characterController, collision,
                entity.transform.position.y < transform.position.y);
            Physics.IgnoreCollision(entity.groundChecker.collider, collision,
                entity.transform.position.y < transform.position.y);
        }
    }

    public void LetEntity(Entity entity)
    {
        Physics.IgnoreCollision(entity.characterController, collision, true);
        Physics.IgnoreCollision(entity.groundChecker.collider, collision, true);
        entitiesWithFreePass.Add(entity);
    }

    private void OnDrawGizmos()
    {
        BoxCollider boxCollider = GetComponent<BoxCollider>();
        boxCollider.size = new Vector3(1f + 0.3f / transform.localScale.x, 1f + 0.3f / transform.localScale.y, 1f + 0.3f / transform.localScale.z);
        transform.localScale = new Vector3(transform.localScale.x, 1, transform.localScale.z);
    }
}
