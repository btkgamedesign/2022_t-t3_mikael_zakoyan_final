using UnityEngine;

public class StationaryEnemy : Entity
{
    GameObject player;
    private void Awake()
    {
        player = GameObject.FindWithTag("Player");
    }

    public override Vector3 Move()
    {
        base.Move();
        transform.LookAt(player.transform);
        return Vector3.zero;
    }
}
