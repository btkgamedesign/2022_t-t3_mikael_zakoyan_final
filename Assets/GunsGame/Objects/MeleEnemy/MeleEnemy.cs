using UnityEngine;

public class MeleEnemy : Entity
{
    [SerializeField] private float speed;
    float directionMul;
    float timeOffset;

    public override void OnStart()
    {
        base.OnStart();
        directionMul = Random.Range(-2f, 2f);
        timeOffset = Random.Range(0f, 100f);
    }

    public override Vector3 Move()
    {
        base.Move();
        var direction = 
            (player.transform.position 
            - transform.position).normalized;
        var strayDir = 
            Vector3.Cross(direction, Vector3.forward).normalized * directionMul
            * (Mathf.Sin(Time.time + timeOffset) * 2 - 1);
        return (direction * speed + strayDir * speed);
    }
}
