using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Corner))]
public class CornerEditor : Editor
{
    private void OnSceneGUI()
    {
        var trg = target as Corner;
        trg.ScaleToParent();
    }
}
