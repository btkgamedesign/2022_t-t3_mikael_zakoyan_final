using UnityEditor;

[CustomEditor(typeof(Platform))]
public class PlatformEditor : Editor
{
    private void OnSceneGUI()
    {
        var trg = target as Platform;
        Corner[] corners = trg.GetComponentsInChildren<Corner>();
        foreach (var corner in corners)
        {
            corner.ScaleToParent();
        }
    }
}
