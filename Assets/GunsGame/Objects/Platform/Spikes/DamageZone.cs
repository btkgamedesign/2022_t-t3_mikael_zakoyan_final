using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityToolbag;

public class DamageZone : MonoBehaviour
{
    [SerializeField] Entity.Team team;
    [SerializeField] int damage;
    [SerializeField] bool doKnockback;
    [ConditionallyVisible("doKnockback")]
    [SerializeField] int direction;
    [ConditionallyVisible("doKnockback")]
    [SerializeField] float time;
    [ConditionallyVisible("doKnockback")]
    [SerializeField] float force;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Entity")
        {
            Entity entity = other.GetComponent<Entity>();
            if (team == entity.team)
                return;
            if (doKnockback)
            {
                entity.InflictDamage(damage, direction, time, force);
            }
            else
            {
                entity.InflictDamage(damage);
            }
        }
    }
}
