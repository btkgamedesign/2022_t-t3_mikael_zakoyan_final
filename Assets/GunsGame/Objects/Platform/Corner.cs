using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corner : MonoBehaviour
{
    [SerializeField] Vector3 size;
    [SerializeField] Side _side;
    public Side side => _side;

    private void Start()
    {
        ScaleToParent(); 
    }

    public void ScaleToParent()
    {
        transform.localScale = new Vector3
            (size.x/transform.parent.localScale.x, 
            size.y/transform.parent.localScale.y, 
            size.z/transform.parent.localScale.z);
    }

    public enum Side
    {
        TL,
        TR,
        BL,
        BR
    }
}
