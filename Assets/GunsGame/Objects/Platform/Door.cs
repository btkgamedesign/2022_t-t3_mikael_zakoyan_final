using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;
    Vector3 cast;

    public void SetCast(Vector3 cast)
    {
        this.cast = cast;
    }

    private void Start()
    {
        IEnumerator CastWithDelay()
        {
            yield return new WaitForFixedUpdate();
            Ray ray = new Ray(transform.position, cast);
            if (Physics.Raycast(ray, cast.magnitude, layerMask))
            {
                gameObject.SetActive(false);
            }
        }

        StartCoroutine(CastWithDelay());
    }

    private void Update()
    {
        
    }
}
