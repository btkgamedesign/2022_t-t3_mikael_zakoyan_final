using UnityEngine;

public class ManaIteam : Iteam
{
    [SerializeField] int mana;

    public override void OnUseEffect(Entity user)
    {
        base.OnUseEffect(user);
        user.AddMana(mana);
    }
}
