using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public Entity owner { get; private set; }
    List<Iteam>[] iteams = new List<Iteam>[10];

    private void Awake()
    {
        owner = GetComponent<Entity>();
        for (int i = 0; i < iteams.Length; i++)
        {
            iteams[i] = new List<Iteam>();
        }
    }

    public void AddIteam(Iteam iteam)
    {
        iteams[iteam.inventorySlot].Add(iteam);
    }

    public void UseIteam(int slot)
    {
        if (iteams[slot].Count > 0)
        {
            iteams[slot][0].Collect(owner, true);
            iteams[slot].RemoveAt(0);
        }
    }
}
