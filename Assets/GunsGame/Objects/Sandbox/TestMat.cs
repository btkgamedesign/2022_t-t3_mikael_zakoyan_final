using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMat : MonoBehaviour
{
    [SerializeField] string variable;
    [SerializeField] Color color;

    Renderer renderer;

    private void Awake()
    {
        renderer = GetComponent<Renderer>();
    }

    private void Update()
    {
        renderer.material.SetColor(variable, color);
    }
}
