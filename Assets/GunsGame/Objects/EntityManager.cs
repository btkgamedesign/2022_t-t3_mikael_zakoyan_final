using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityManager : MonoBehaviour
{
    public static EntityManager singleton;

    private List<Entity> allEntities = new List<Entity>();

    private void Awake()
    {
        singleton = this;
    }

    public void AddEntity(Entity entity)
    {
        allEntities.Add(entity);
    }

    public void RemoveEntity(Entity entity)
    {
        allEntities.Remove(entity);
    }
}
