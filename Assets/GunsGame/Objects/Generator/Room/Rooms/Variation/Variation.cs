using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Variation : MonoBehaviour
{
    public void PickEntities()
    {
        Randomizer randomizer = FindObjectOfType<Randomizer>();
        EntitiesVariation[] entitiesVariation = GetComponentsInChildren<EntitiesVariation>(true);
        int roll = randomizer.Int(entitiesVariation.Length);
        for (int i = 0; i < entitiesVariation.Length; i++)
        {
            entitiesVariation[i].gameObject.SetActive(i == roll);
        }
    }

    public void RemoveEntities()
    {
        Entity[] entities = GetComponentsInChildren<Entity>();
        foreach (var entity in entities)
        {
            Destroy(entity.gameObject);
        }
    }
}
