using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Variations : MonoBehaviour
{
    Variation[] variations;
    [SerializeField] bool show;
    Randomizer randomizer;
    int roll;

    private void Awake()
    {
        randomizer = FindObjectOfType<Randomizer>();
        variations = GetComponentsInChildren<Variation>(true);
        roll = randomizer.Int(variations.Length - 1);
        for (int i = 0; i < variations.Length; i++)
        {
            variations[i].gameObject.SetActive(i == roll);
            if (i == roll)
            {
                variations[i].PickEntities();
            }
        }
        variations[roll].gameObject.SetActive(true);
        variations[roll].PickEntities();
        show = true;
    }

    public void RemoveEntities()
    {
        variations[roll].RemoveEntities();
    }
}
