using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomPhysical : MonoBehaviour
{
    public Vector2Int size;
    [SerializeField] GameObject cell;
    Snapper snapper;
    BoxCollider2D boxCollider;

    private void Awake()
    {
        snapper = GetComponentInChildren<Snapper>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    public Vector3 getWorldPos(Vector3 size)
    {
        Vector3 grid = new Vector3(Mathf.Round(snapper.transform.position.x), Mathf.Round(snapper.transform.position.y));
        Vector3 world = Vector3.Lerp(Vector3.Scale(grid, size), Vector3.Scale(grid + new Vector3(this.size.x, this.size.y), size), 0.5f);
        return world;
    }

    public void Initialize()
    {
        int width = size.x;
        int height = size.y;
        boxCollider.offset = new Vector2(width, height) / 2 - new Vector2(0.5f, 0.5f);
        boxCollider.size = new Vector2(width, height);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GameObject newCell = Instantiate(cell, snapper.transform);
                newCell.transform.localPosition = new Vector3(x, y, 0);
            }
        }
    }

    public bool hasNeighbours()
    {
        Vector3[] directions = new Vector3[4]
        {
            Vector3.left,
            Vector3.right,
            Vector3.up,
            Vector3.down
        };
        bool result = false;
        foreach (var dir in directions)
        {
            Vector3 offst = Vector3.Scale(dir, new Vector3(size.x/2 + 0.1f, size.y/2 + 0.1f));
            Ray ray = new Ray(transform.position + offst, dir);
            bool cast = Physics2D.Raycast(transform.position + dir, dir);
            if (cast)
            {
                result = true;
                break;
            }
        }
        return result;
    }
}
