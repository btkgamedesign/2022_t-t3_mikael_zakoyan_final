using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Generator : MonoBehaviour
{
    Randomizer random;
    GridArranger gridArranger;
    [SerializeField] GameObject physicalRoom;
    Vector2Int[] sizes;
    int sizesShuffler;
    [SerializeField] float radiusX;
    [SerializeField] float radiusY;
    [SerializeField] MinMaxInt numberOfRooms;
    Rigidbody2D[] roomSimulations;

    bool simulationDone = false;

    private void Awake()
    {
        Generate();
    }

    public void Generate()
    {
        List<Vector2Int> roomSizes = new List<Vector2Int>();
        gridArranger = GetComponent<GridArranger>();
        foreach (var realRoom in gridArranger.rooms)
        {
            Vector2Int siz = realRoom.GetComponent<Room>().size;
            if (!roomSizes.Contains(siz))
            {
                roomSizes.Add(siz);
            }
        }
        sizes = roomSizes.ToArray();

        IEnumerator Generate()
        {
            Time.timeScale = 100;
            random = GetComponent<Randomizer>();
            random.Initialize();
            sizesShuffler = random.CreateShuffler(sizes.Length);
            int rooms = random.Int(numberOfRooms);
            for (int i = 0; i < rooms; i++)
            {
                Vector2Int size = sizes[random.Int(random.Shuffle(sizesShuffler))];
                RoomPhysical roomPhysical = Instantiate(physicalRoom, transform).GetComponent<RoomPhysical>();
                roomPhysical.size = size;
                roomPhysical.transform.position = GetRandomPointInCircle();
                roomPhysical.Initialize();
            }
            roomSimulations = GetComponentsInChildren<Rigidbody2D>();

            yield return new WaitUntil(() => simulationDone);

            Time.timeScale = 1;
            Dictionary<Vector2Int, Cell> cellsGrid = new Dictionary<Vector2Int, Cell>();
            Cell[] cells = GetComponentsInChildren<Cell>();
            foreach (var cell in cells)
            {
                var key = new Vector2Int((int)cell.transform.position.x, (int)cell.transform.position.y);
                if (cellsGrid.ContainsKey(key))
                {
                    continue;
                }
                cellsGrid.Add(key, cell);
            }

            gridArranger = GetComponent<GridArranger>();
            gridArranger.Initialize(cellsGrid);

            LevelManager levelManager = FindObjectOfType<LevelManager>();
            levelManager.NotifyDoneGenerating();
        }

        StartCoroutine(Generate());
    }

    private void Update()
    {
        if (!simulationDone)
        {
            bool someoneIsAwake = false;
            foreach (var simulation in roomSimulations)
            {
                if (!simulation.IsSleeping())
                {
                    someoneIsAwake = true;
                    break;
                }
            }
            if (!someoneIsAwake)
            {
                simulationDone = true;
            }
        }

    }

    Vector2 GetRandomPointInCircle()
    {
        int angle = random.Int(360);
        float x = radiusX * random.Float() * Mathf.Cos(angle);
        float y = radiusY * random.Float() * Mathf.Sin(angle);
        return new Vector2(x, y);
    }

    public void ClearAll()
    {
        print(transform.childCount);
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
}

