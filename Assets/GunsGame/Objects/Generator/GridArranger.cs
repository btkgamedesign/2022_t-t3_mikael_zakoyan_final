using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GridArranger : MonoBehaviour
{
    Randomizer random;
    Dictionary<Vector2Int, Cell> cellsGrid;

    [SerializeField] GameObject[] _rooms;
    public GameObject[] rooms => _rooms;
        
    Dictionary<Vector2Int, List<GameObject>> indexedRooms = new Dictionary<Vector2Int, List<GameObject>>();

    public UnityEvent onGenerationDone;

    private void Update()
    {
        
    }

    public void Initialize(Dictionary<Vector2Int, Cell> cellsGrid)
    {
        random = GetComponent<Randomizer>();
        this.cellsGrid = cellsGrid;
        IndexRooms();

        RoomPhysical[] physicalRooms = GetComponentsInChildren<RoomPhysical>();
        int i = 0;
        foreach (var physicalRoom in physicalRooms)
        {
            bool hasNeighbours = physicalRoom.hasNeighbours();
            if (hasNeighbours)
            {
                int chosenRoomID = random.Int(indexedRooms[physicalRoom.size].Count);
                GameObject roomToInstantiate = indexedRooms[physicalRoom.size][chosenRoomID];
                GameObject roomInstance = Instantiate(roomToInstantiate, transform);
                roomInstance.transform.position = physicalRoom.getWorldPos(Room.roomSize);
                roomInstance.name = i.ToString();
            }
            Destroy(physicalRoom.gameObject);
            i++;
        }
        StartCoroutine(DoneWithDelay());

        IEnumerator DoneWithDelay()
        {
            yield return null;
            yield return null;
            yield return null;
            yield return null;
            onGenerationDone?.Invoke();
        }
    }

    void IndexRooms()
    {
        foreach (var roomObject in rooms)
        {
            Room room = roomObject.GetComponent<Room>();
            if (!indexedRooms.ContainsKey(room.size))
            {
                indexedRooms.Add(room.size, new List<GameObject>());
            }
            indexedRooms[room.size].Add(roomObject);
        }
    }

}

