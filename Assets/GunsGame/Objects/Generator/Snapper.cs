using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snapper : MonoBehaviour
{
    [SerializeField] Vector2 gridSize;
    [SerializeField] bool floor;

    private void Update()
    {
        if (floor)
        {
            transform.position = FloorToGrid(transform.parent.position, gridSize);
        }
        else
        {
            transform.position = SnapToGrid(transform.parent.position, gridSize);
        }
    }

    public static Vector3 SnapToGrid(Vector3 pos, Vector2 grid, float z = 0)
    {
        return new Vector3(
            Mathf.RoundToInt(pos.x / grid.x) * grid.x,
            Mathf.RoundToInt(pos.y / grid.y) * grid.y,
            z);
    }

    public static Vector3 FloorToGrid(Vector3 pos, Vector2 grid, float z = 0)
    {
        return new Vector3(
            Mathf.FloorToInt(pos.x / grid.x) * grid.x,
            Mathf.FloorToInt(pos.y / grid.y) * grid.y,
            z);
    }

    private void OnDrawGizmos()
    {
        var boxCollider = GetComponentInParent<BoxCollider2D>();
        Gizmos.color = Color.black;
        Gizmos.DrawWireCube(transform.position + (Vector3)boxCollider.offset, (Vector2)boxCollider.size);
    }
}
