using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastCatcher : MonoBehaviour
{
    [SerializeField] LayerMask layer;
    BoxCollider boxCollider;
    
    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    public bool Cast(Vector3 direction)
    {
        bool result = false;
        direction.Normalize();
        Ray ray = new Ray(transform.position , direction);
        Debug.DrawRay(ray.origin, ray.direction * Vector3.Scale(direction, Room.roomSize).magnitude, Color.red, 10f);
        RaycastHit[] hits = Physics.RaycastAll(ray, Vector3.Scale(direction, Room.roomSize).magnitude, layer);
        foreach (var hit in hits)
        {
            if (hit.collider == boxCollider)
            {
                continue;
            }
            
            result = true;
            Debug.DrawRay(ray.origin, direction * 1, Color.blue, 10f);
            Debug.DrawRay(hit.point, -direction * 1, Color.cyan, 10f);
            break;
        }
        return result;
    }
}
