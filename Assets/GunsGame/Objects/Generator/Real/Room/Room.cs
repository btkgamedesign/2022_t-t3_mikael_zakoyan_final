using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public bool showGizmos;
    public static Vector3 roomSize = new Vector3(17, 11, 1);
    public static float doorWidth = 4f;
    public Vector2Int size;

    [SerializeField] GameObject cameraTriggerPrefab;

    Variations variations;

    public bool removeEntitiesOnStart { get; set; }

    private void Awake()
    {
        showGizmos = false;
        Vector3[] cellCenters = getCellCenters();

        DoorCameraTrigger cameraTrigger = Instantiate(cameraTriggerPrefab, transform).GetComponent<DoorCameraTrigger>();
        cameraTrigger.Set(new Vector3(roomSize.x * size.x, roomSize.y * size.y, 1f));

        variations = GetComponentInChildren<Variations>();
    }

    private void Start()
    {
        if (removeEntitiesOnStart)
        {
            RemoveEntities();
        }
    }

    public Vector3 getSpawnPos(float side)
    {
        return transform.position;
        float x = -size.x / 2 * roomSize.x + side * roomSize.x;
        Ray ray = new Ray(transform.position + Vector3.right * x, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100f))
        {
            return hit.point;
        }
        return Vector3.zero;
    }

    Vector3 getCellPos(int x, int y)
    {
        Vector3 offst = -new Vector3((size.x - 1) * roomSize.x, (size.y - 1) * roomSize.y) / 2f;
        return new Vector3(x * roomSize.x, y * roomSize.y) + offst + transform.position;
    }

    Vector3 getDirOffset(Vector3 direction)
    {
        return Vector3.Scale(direction , roomSize / 2);
    }

    Vector3[] getCellCenters()
    {
        List<Vector3> result = new List<Vector3>();
        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                Gizmos.color = Color.grey;
                Vector3 pos = getCellPos(x, y);
                result.Add(pos);
            }
        }
        return result.ToArray();
    }

    public Vector3[] GetExitLocations()
    {
        List<Vector3> result = new List<Vector3>();
        CellPhysical[] cells = GetComponentsInChildren<CellPhysical>();
        foreach (var cell in cells)
            result.AddRange(cell.GetDoorPositions());
        return result.ToArray();
    }

    public void Activate()
    {
        variations.gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        variations.gameObject.SetActive(false);
    }

    public void RemoveEntities()
    {
        variations.RemoveEntities();
    }
    private void OnDrawGizmos()
    {
        if (!showGizmos)
        {
            return;
        }
        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                Gizmos.color = Color.grey;
                Vector3 pos = getCellPos(x, y);
                Gizmos.DrawWireCube(pos, roomSize);

                Gizmos.color = new Color(0,1,0,0.15f);
                if (x == 0)
                {
                    Gizmos.DrawWireCube(pos + getDirOffset(Vector3.left), Vector3.one * doorWidth);
                }
                if (x == size.x - 1)
                {
                    Gizmos.DrawWireCube(pos + getDirOffset(Vector3.right), Vector3.one * doorWidth);
                }
                if (y == 0)
                {
                    Gizmos.DrawWireCube(pos + getDirOffset(Vector3.down), Vector3.one * doorWidth);
                }
                if (y == size.y - 1)
                {
                    Gizmos.DrawWireCube(pos + getDirOffset(Vector3.up), Vector3.one * doorWidth);
                }
            }
        }

    }
}
