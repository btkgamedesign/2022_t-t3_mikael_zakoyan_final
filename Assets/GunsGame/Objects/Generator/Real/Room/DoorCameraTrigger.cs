using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCameraTrigger : MonoBehaviour
{
    Room room;
    MainCamera cam => LevelManager.levelManager.mainCamera;
    Vector3 size;

    private void Awake()
    {
        room = GetComponentInParent<Room>();
    }

    public void Set(Vector3 size)
    {
        transform.localScale = new Vector3(size.x - 0.5f, size.y - 1f, 20f);
        this.size = size;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "CameraTrigger")
        {
            cam.minX = transform.position.x - size.x / 2;
            cam.maxX = transform.position.x + size.x / 2;
            cam.minY = transform.position.y - size.y / 2;
            cam.maxY = transform.position.y + size.y / 2;

            LevelManager.levelManager.SetActiveRoom(room);
        }
    }
}
