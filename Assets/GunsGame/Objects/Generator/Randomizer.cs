using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Randomizer : MonoBehaviour
{
    [SerializeField] string stringSeed;
    [SerializeField] bool randomSeed;
    System.Random random = null;
    public int resultedSeed;
    List<Shuffler> shufflers = new List<Shuffler>();

    public int CreateShuffler(int size)
    {
        Shuffler newShuffler = new Shuffler(size);
        shufflers.Add(newShuffler);
        return shufflers.Count - 1;
    }

    public int Shuffle(int id)
    {
        Shuffler shuffler = shufflers[id];
        List<int> listWithoutLast = new List<int>();
        listWithoutLast.AddRange(shuffler.fullArray);
        if (shuffler.lastResult != -1)
        {
            listWithoutLast.Remove(shuffler.lastResult);
        }
        int result = listWithoutLast[Int(listWithoutLast.Count)];
        shufflers[id].lastResult = result;
        return result;
    }

    public bool Roll(float percentage)
    {
        int roll = Int(1000);
        bool result = roll < percentage * 1000;
        return result;
    }

    public int Int(MinMaxInt minMax)
    {
        return random.Next(minMax.min, minMax.max);
    }

    public int Int(int min, int max)
    {
        return random.Next(min, max);
    }

    public int Int(int max)
    {
        if (max <= 0)
        {
            return 0;
        }
        return random.Next(max);
    }

    public float Float()
    {
        return (float)random.NextDouble();
    }

    int stringToInt(string str)
    {
        int result = 0;
        foreach (var chr in str)
        {
            int nummered = chr - '0';
            result += nummered;
        }
        return result;
    }

    public void Initialize()
    {
        int seed;
        if (!int.TryParse(stringSeed, out seed))
        {
            seed = stringToInt(stringSeed);
        }
        if (randomSeed)
        {
            seed = UnityEngine.Random.Range(0, 1000000);
        }
        random = new System.Random(seed);
        resultedSeed = seed;
    }

    class Shuffler
    {
        public int[] fullArray;
        public int lastResult = -1;

        public Shuffler(int size)
        {
            fullArray = new int[size];
            for (int i = 0; i < size; i++)
            {
                fullArray[i] = i+1;
            }
        }
    }
}
