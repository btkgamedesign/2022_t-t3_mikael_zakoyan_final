using UnityEngine;
using UnityToolbag;

public class Weapon : MonoBehaviour
{
    public Entity owner { get; set; }
    public bool hasOwner => owner != null;
    [SerializeField] private int _slot = 0;

    [SerializeField] private Entity.WeaponAnim _weaponAnim = Entity.WeaponAnim.Pistol;
    public Entity.WeaponAnim weaponAnim => _weaponAnim;

    [SerializeField] Transform visual;
    [SerializeField] AnimationCurve reloadCurve;
    public int slot => _slot;
    [SerializeField] private int burst = 1;
    [SerializeField] private float spread = 0.0f;
    [SerializeField] private float cooldown = 0.3f;
    public float getCooldown => cooldown;
    private float currentCooldown;
    [SerializeField] private GameObject projectile = null;
    [SerializeField] private Transform _source = null;
    public Transform overrideSource { get; set; }
    public Transform source => overrideSource ? overrideSource : _source;
    [SerializeField] private bool isAutomatic = false;
    [SerializeField] private int bullets = 6;
    [SerializeField] bool infiniteBullet = false;
    
    private int _currentBullets;
    private int _currentBulletsBeforeReload;
    int currentBullets
    {
        get => _currentBullets;
        set
        {
            _currentBullets = Mathf.Clamp(value, 0, bullets);
        }
    }
    bool isShooting;
    [SerializeField] float reloadTime = 0.3f;
    [SerializeField] bool automaticReload = false;
    [SerializeField] bool reloadBulletByBullet = false;
    float currentReloadTime;
    public bool isReloading => currentReloadTime >= 0.0f;

    [SerializeField] bool freezeOwnerWhenShooting = false;
    public bool freezeOwner => currentFreezeTime >= 0f | (currentFreezeReloadTime >= 0f & freezeOwnerWhenReloading);
    [ConditionallyVisible("freezeOwnerWhenShooting")]
    [SerializeField] float freezeTime = 0.2f;
    float currentFreezeTime;

    [SerializeField] bool freezeOwnerWhenReloading = false;
    float freezeReloadTime => reloadTime;
    float currentFreezeReloadTime;

    private void Start()
    {
        currentBullets = bullets;
    }

    private void Update()
    {
        if (isShooting)
        {
            Shoot();
        }

        if (isReloading)
        {
            float reloadProgress = reloadCurve.Evaluate(1 - currentFreezeReloadTime / freezeReloadTime);
            visual.transform.localEulerAngles = new Vector3(360 * reloadProgress, 0, 0);
        }
        else
        {
            visual.transform.localRotation = Quaternion.identity;
        }
        currentCooldown -= Time.deltaTime;
        currentReloadTime -= Time.deltaTime;
        currentFreezeTime -= Time.deltaTime;
        currentFreezeReloadTime -= Time.deltaTime;
    }

    public void PressTrigger()
    {
        if (!hasOwner) return;

        if (isAutomatic)
        {
            isShooting = true;
        }
        else
        {
            Shoot();
        }
    }

    public void ReleaseTrigger()
    {
        if (!hasOwner) return;

        if (isAutomatic)
        {
            isShooting = false;
        }
    }

    void Shoot()
    {
        if (currentBullets <= 0 & automaticReload)
        {
            Reload();
        }
        if (currentCooldown > 0 | currentBullets <= 0 | isReloading)
        {
            return;
        }
        currentCooldown = cooldown;
        if (!infiniteBullet)
        {
            currentBullets--;
        }
        for (int i = 0; i < burst; i++)
        {
            var instance = Instantiate(projectile, source).GetComponent<Projectile>();
            if (hasOwner)
            {
                instance.damage = (int)(instance.damage * owner.arsenal.damageMultiplyer);
                instance.critDamage = (int)(instance.critDamage * owner.arsenal.damageMultiplyer);
                instance.force = (int)(instance.force * owner.arsenal.damageMultiplyer);
            }
            instance.owner = owner;
            instance.transform.Rotate
                (new Vector3
                (Random.Range(-spread / 2, spread / 2), 0, 0f));
            instance.transform.SetParent(null);
        }

        if (freezeOwnerWhenShooting)
        {
            currentFreezeTime = freezeTime;
        }
    }

    public void Reload()
    {
        if (!hasOwner) return;

        if (isReloading)
        {
            return;
        }
        currentReloadTime = reloadTime;
        _currentBulletsBeforeReload = currentBullets;
        if (reloadBulletByBullet)
        {
            currentBullets++;
        }
        else
        {
            currentBullets = bullets;
        }

        currentFreezeReloadTime = freezeReloadTime;
    }

    public void CancelReload()
    {
        if (!isReloading) return;
        currentBullets = _currentBulletsBeforeReload;
        currentReloadTime = -1f;
        print(isReloading);
    }

    private void OnDisable()
    {
        ReleaseTrigger();
    }
}
