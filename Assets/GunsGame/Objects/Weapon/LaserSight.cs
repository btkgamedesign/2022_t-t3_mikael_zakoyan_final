using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSight : MonoBehaviour
{
    Weapon weapon;
    public Transform source => weapon.source;
    [SerializeField] float _maxDistance;
    public float maxDistance => _maxDistance;
    [SerializeField] LayerMask _layerMask;
    public LayerMask layerMask => _layerMask;

    LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        weapon = GetComponentInParent<Weapon>();
    }

    private void Update()
    {
        lineRenderer.SetPosition(0, source.transform.position);
        Vector3 endPos = source.transform.position + (source.transform.rotation * Vector3.forward) * maxDistance;
        Ray ray = new Ray(source.transform.position, source.transform.rotation * Vector3.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, maxDistance, layerMask))
        {
            endPos = hit.point;
        }
        lineRenderer.SetPosition(1, endPos);

        if (weapon.owner != null)
        {
            lineRenderer.enabled = weapon.owner.isAiming;
        }
    }
}
