using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelStats : MonoBehaviour
{
    Player player => LevelManager.levelManager.player;
    LevelManager levelManager => LevelManager.levelManager;
    [SerializeField] TMP_Text _enemies;
    [SerializeField] TMP_Text _player;

    public void Update()
    {
        if (LevelManager.levelManager == null)
            return;
        if (player == null)
            return;
        if (!player.hasUpgrader)
            return;

        _player.text = "health " + player.upgrader.health.ToString() + "\n" +
                       "damage " + player.upgrader.damage.ToString() + "\n" +
                       "speed " + player.upgrader.speed.ToString() + "\n";

        _enemies.text = "health " + levelManager.levelHealth.ToString() + "\n" +
                        "damage " + levelManager.levelDamage.ToString() + "\n" +
                        "speed " + levelManager.levelSpeed.ToString() + "\n";
    }
}
