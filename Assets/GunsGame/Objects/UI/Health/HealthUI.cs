using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthUI : MonoBehaviour
{
    Player player => LevelManager.levelManager.player;
    [SerializeField] TMP_Text text;
    [SerializeField] Image foreground;

    private void Update()
    {
        if (player == null)
        {
            text.text = "HP 0";
            foreground.fillAmount = 0f;
        }
        else
        {
            text.text = "HP " + player.health;
            foreground.fillAmount = (float)player.health / (float)player.maxHealth;
        }
    }
}
