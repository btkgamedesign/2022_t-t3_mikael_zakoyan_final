using UnityEngine;

public class Iteam : MonoBehaviour
{
    public int inventorySlot;

    public void Collect(Entity user, bool forceUse = false)
    {
        if (user.hasInventory & !forceUse)
        {
            user.inventory.AddIteam(this);
            gameObject.SetActive(false);
            print("Collected iteam " + name);
        }
        else
        {
            OnUseEffect(user);
            Destroy(gameObject);
            print("Used iteam " + name);
        }
    }

    public virtual void OnUseEffect(Entity user)
    { }
}
