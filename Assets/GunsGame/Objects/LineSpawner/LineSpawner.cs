using UnityEngine;

public class LineSpawner : MonoBehaviour, IEntitySpawner
{
    [SerializeField] private Transform lineEnd;

    public void Spawn(GameObject entity)
    {
        var instance = Instantiate(entity);
        instance.transform.position = GetSpawnPos();
    }

    Vector3 GetSpawnPos() => Vector3.Lerp
        (transform.position, lineEnd.transform.position, 
        Random.Range(0f, 1f));
    

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, lineEnd.transform.position);
    }

    [SerializeField] private GameObject sample;
    public void SpawnSample() => Spawn(sample);
    
}
