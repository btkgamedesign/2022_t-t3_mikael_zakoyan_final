using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LineSpawner))]
public class LineSpawnerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI(); 
        var trg = target as LineSpawner;
        if (GUILayout.Button("Spawn sample"))
        {
            trg.SpawnSample();
        }
    }
}
