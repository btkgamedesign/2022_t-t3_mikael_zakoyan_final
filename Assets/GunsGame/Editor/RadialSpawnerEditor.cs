using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RadialSpawner))]
public class RadialSpawnerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var trg = target as RadialSpawner;
        if (GUILayout.Button("Spawn sample"))
        {
            trg.SpawnSample();
        }
    }
}
