using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearChance : MonoBehaviour
{
    [Range(0f,1f)]
    [SerializeField] float percentage;
    private void Start()
    {
        bool canExist = LevelManager.levelManager.randomizer.Roll(percentage);
        if (!canExist)
        {
            Destroy(gameObject);
        }
    }
}
