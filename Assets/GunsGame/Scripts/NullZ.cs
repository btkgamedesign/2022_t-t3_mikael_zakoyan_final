using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NullZ
{
    public static void Do(Transform target)
    {
        target.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, 0);
    }

    public static void Scale(Transform scale)
    {
        Vector3 scl = scale.transform.lossyScale;
        scale.localScale = new Vector3(Round(scale.localScale.x), Round(scale.localScale.y), 1);
    }

    public static Vector3 Snap(Transform snap)
    {
        Vector3 pos = snap.position;
        return new Vector3((int)(pos.x), (int)(pos.y), 0);
    }


    static float Round(float num)
    {
        return (float)Math.Round(num, MidpointRounding.AwayFromZero) / 2;
    }
}
