using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snap : MonoBehaviour
{
    [SerializeField] float zOffset;
    Vector3 positionSize = new Vector3(0.5f, 0.5f, 0.5f);

    private void OnDrawGizmos()
    {
        SnapToGrid();
    }

    void SnapToGrid()
    {
        Vector3 pos = new Vector3(
            Mathf.Round(transform.position.x / positionSize.x) * positionSize.x,
            Mathf.Round(transform.position.y / positionSize.y) * positionSize.y,
            zOffset);
        transform.position = pos;

        Vector3 scl = new Vector3(
            Mathf.RoundToInt(transform.localScale.x),
            Mathf.RoundToInt(transform.localScale.y),
            1);
        transform.localScale = scl;
    }
}
