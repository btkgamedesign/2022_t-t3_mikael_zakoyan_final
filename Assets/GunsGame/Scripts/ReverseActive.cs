using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReverseActive : MonoBehaviour
{
    [SerializeField] GameObject reference;
    [SerializeField] GameObject target;

    private void Update()
    {
        target.SetActive(!reference.activeSelf);
    }
}
