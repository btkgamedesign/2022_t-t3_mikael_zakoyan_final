using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshVariation : MonoBehaviour
{
    MeshFilter meshFilter;
    SkinnedMeshRenderer skinnedMeshRenderer;
    [SerializeField] Mesh[] _meshes;
    public Mesh[] meshes => _meshes;
    public int currentMesh;

    public void SetMesh(int mesh)
    {
        if (mesh < 0 || mesh >= _meshes.Length)
        {
            return;
        }
        if (_meshes[mesh] == null)
        {
            return;
        }

        meshFilter = GetComponent<MeshFilter>();
        if (meshFilter)
        {
            meshFilter.mesh = _meshes[mesh];
            return;
        }
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        if (skinnedMeshRenderer)
        {
            skinnedMeshRenderer.sharedMesh = _meshes[mesh];
        }
    }
}
