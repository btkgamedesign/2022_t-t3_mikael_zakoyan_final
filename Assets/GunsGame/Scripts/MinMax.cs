using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct MinMaxInt
{
    public int min;
    public int max;
}

[Serializable]
public struct MinMaxFloat
{
    public float min;
    public float max;
}

public static class MinMax
{
    public static int GetRandom(MinMaxInt minMax)
    {
        return UnityEngine.Random.Range(minMax.min, minMax.max);
    }

    public static float GetRandom(MinMaxFloat minMax)
    {
        return UnityEngine.Random.Range(minMax.min, minMax.max);
    }
}