using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshVariation))]
public class MeshVariationEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var trg = target as MeshVariation;
        if (trg.meshes != null)
        {
            trg.currentMesh = Mathf.Clamp(trg.currentMesh, 0, trg.meshes.Length - 1);
            trg.SetMesh(trg.currentMesh);
        }
    }

}
