using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepScale : MonoBehaviour
{
    [SerializeField] Vector3 scale = new Vector3(1,1,1);
    [SerializeField] Transform center;
    [SerializeField] Transform top;
    [SerializeField] Vector3 topDefaultPos;

    private void OnDrawGizmos()
    {
        if (center == null || top == null)
        {
            return;
        }

        Vector3 parScl = transform.parent.localScale;
        transform.localScale = new Vector3(scale.x / parScl.x, scale.y / parScl.y, scale.z / parScl.z);
        center.transform.localScale = parScl;

        top.transform.localPosition = new Vector3(topDefaultPos.x, center.transform.localScale.y * topDefaultPos.y * parScl.y, topDefaultPos.z);
    }

}
